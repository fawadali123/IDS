﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImageGallery.Model
{
   public class RevokeAccessViewModel
    {
        public string SubjectId { get; set; }
        public string ClientId { get; set; }
    }
}
