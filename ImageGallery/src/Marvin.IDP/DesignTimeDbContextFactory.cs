﻿using Marvin.IDP.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Marvin.IDP
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<MarvinUserContext>
    {
        public MarvinUserContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            var builder = new DbContextOptionsBuilder<MarvinUserContext>();
            var connectionString = configuration.GetConnectionString("marvinUserDBConnectionString");
            builder.UseSqlServer(connectionString);
            return new MarvinUserContext(builder.Options);
        }
    }
}
