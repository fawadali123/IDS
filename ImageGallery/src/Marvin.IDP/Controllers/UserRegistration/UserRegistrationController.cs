﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer4.Services;
using Marvin.IDP.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace CS.Security.IDS.Controllers.UserRegistration
{
    public class UserRegistrationController : Controller
    {
        private readonly IMarvinUserRepository _marvinUserRepository;
        private readonly IIdentityServerInteractionService _interaction;

        public UserRegistrationController(IMarvinUserRepository marvinUserRepository, IIdentityServerInteractionService interaction)
        {
            _marvinUserRepository = marvinUserRepository;
            _interaction = interaction;
        }
        [HttpGet]
        public IActionResult RegisterUser(string returnUrl)
        {
            var vm = new RegisterUserViewModel()
            {
                ReturnUrl = returnUrl
            };
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RegisterUser(RegisterUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userToCreate = new Marvin.IDP.Entities.User();
                userToCreate.Password = model.Password;
                userToCreate.Username = model.Username;
                userToCreate.IsActive = true;
                userToCreate.Claims.Add(new Marvin.IDP.Entities.UserClaim("country", model.Country));
                userToCreate.Claims.Add(new Marvin.IDP.Entities.UserClaim("address", model.Address));
                userToCreate.Claims.Add(new Marvin.IDP.Entities.UserClaim("given_name", model.FirstName));
                userToCreate.Claims.Add(new Marvin.IDP.Entities.UserClaim("family_name", model.LastName));
                userToCreate.Claims.Add(new Marvin.IDP.Entities.UserClaim("email", model.Email));
                userToCreate.Claims.Add(new Marvin.IDP.Entities.UserClaim("subscriptionlevel", "FreeUser"));
                _marvinUserRepository.AddUser(userToCreate);

                if (!_marvinUserRepository.Save())
                {
                    throw new Exception($"Creating a user failed.");
                }
                var claims = new List<Claim>
                {
                    new Claim("country", model.Country),
                    new Claim("address", model.Address),
                    new Claim("given_name", model.FirstName),
                    new Claim("family_name", model.LastName),
                    new Claim("email", model.Email),
                    new Claim("subscriptionlevel", "FreeUser"),
                    new Claim("sub",userToCreate.SubjectId)
                };
                //HttpContext.SignInAsync(userToCreate.SubjectId,model)
                var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var principal = new ClaimsPrincipal(identity);

                await HttpContext.SignInAsync(principal);
                //await LoginAsync(model);
                //if(_interaction.IsValidReturnUrl(model.ReturnUrl) || Url.IsLocalUrl(model.ReturnUrl))
                //{
                //  return Redirect(model.ReturnUrl);
                // }
                return Redirect("~/");
            }
            return View(model);
        }

        private async Task LoginAsync(RegisterUserViewModel model)
        {
            var claims = new List<Claim>
                {
                    new Claim("country", model.Country),
                    new Claim("address", model.Address),
                    new Claim("given_name", model.FirstName),
                    new Claim("family_name", model.LastName),
                    new Claim("email", model.Email),
                    new Claim("subscriptionlevel", "FreeUser"),
                };
            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(identity);
            await HttpContext.SignInAsync(principal);
        }
    }
}