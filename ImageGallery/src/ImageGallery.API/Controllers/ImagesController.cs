﻿using AutoMapper;
using IdentityModel.Client;
using ImageGallery.API.Helpers;
using ImageGallery.API.Services;
using ImageGallery.Model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ImageGallery.API.Controllers
{
    [Route("api/images")]
    [Authorize]
    public class ImagesController : Controller
    {
        private readonly string IDP_BASE_URL = "https://localhost:44379";
        private readonly IGalleryRepository _galleryRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        
        public ImagesController(IGalleryRepository galleryRepository,
            IHostingEnvironment hostingEnvironment)
        {
            _galleryRepository = galleryRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<IActionResult> RevokeAccessToken(string accessToken)
        {
            var discoveryClient = new IdentityModel.Client.DiscoveryClient($"{IDP_BASE_URL}");
            var metaDataResponse = discoveryClient.GetAsync();
            var revocationClient = new TokenRevocationClient(metaDataResponse.Result.RevocationEndpoint, "angular_spa", "apisecret");

            var response = await revocationClient.RevokeAccessTokenAsync(accessToken);
            
            if (!response.IsError)
            {
                await HttpContext.SignOutAsync("Cookies");
                await HttpContext.SignOutAsync("oidc");
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }
        public async Task<IActionResult> IntrospectClientToken()
        {
            var discoveryClient = new IdentityModel.Client.DiscoveryClient($"{IDP_BASE_URL}");
            var metaDataResponse = discoveryClient.GetAsync();
            var introspectionClient = new IntrospectionClient(metaDataResponse.Result.IntrospectionEndpoint, "imagegalleryapi", "apisecret");
            var token = HttpContext.GetTokenAsync("access_token").Result;
            var response = await introspectionClient.SendAsync(
                new IntrospectionRequest { Token = token });

            var isActive = response.IsActive;
            var claims = response.Claims;
            return Ok();
        }
        [HttpGet("UserClaims")]
        [Authorize("CanAccessResource")]
        public async Task<IActionResult> UserClaims()
        {
            var x = await IntrospectClientToken();
        
            //Task abc = WriteOutIdentityInformation();
            // get the saved identity token
            var identityToken = HttpContext.GetTokenAsync(OpenIdConnectParameterNames.IdToken);
            var accessToken = HttpContext.GetTokenAsync("access_token").Result;


            // get the metadata


            // create a TokenRevocationClient
            var discoveryClient = new IdentityModel.Client.DiscoveryClient($"{IDP_BASE_URL}");
            var metaDataResponse = discoveryClient.GetAsync();
            var revocationClient = new IdentityModel.Client.TokenRevocationClient(metaDataResponse.Result.RevocationEndpoint, "angular_spa");

            if (!string.IsNullOrWhiteSpace(accessToken))
            {
                var revokeAccessTokenResponse = revocationClient.RevokeAccessTokenAsync("a0a0d239130b1f89aec802047f252d4377649ae791c63bc948320d41abaca65d").Result;

               // if (revokeAccessTokenResponse.IsError)
               // {
                    //throw new Exception("Problem encountered while revoking the access token."
                       // , revokeAccessTokenResponse.Exception);
                //}
            }
            /*
            var doc = new IdentityModel.Client.DiscoveryClient("https://localhost:44379/");
            var introspectionClient = new IntrospectionClient(
                "https://localhost:44379/connect/introspect",
                "imagegalleryapi",
                "apisecret");

            var response = introspectionClient.SendAsync(new IntrospectionRequest { Token = accessToken.Result });

            var isActive = response.Result.IsActive;
            var claims = response.Result.Claims;
            */

            // write out the user claims
            foreach (var claim in User.Claims)
            {
                Debug.WriteLine($"Claim type: {claim.Type} - Claim value: {claim.Value}");
            }
            List<object> claimPermissions = new List<object>();

            foreach (var item in User.Claims)
            {
                claimPermissions.Add(new
                {
                    Type = item.Type,
                    Value = item.Value
                });
            }
            return Ok(claimPermissions);
        }

        [HttpGet()]
        public IActionResult GetImages()
        {
            var ownerId = User.Claims.FirstOrDefault(c => c.Type == "sub").Value;

            // get from repo
            var imagesFromRepo = _galleryRepository.GetImages(ownerId);

            // map to model
            var imagesToReturn = Mapper.Map<IEnumerable<Model.Image>>(imagesFromRepo);

            // return
            return Ok(imagesToReturn);
        }

        [HttpGet("{id}", Name = "GetImage")]
        [Authorize("MustOwnImage")]
        public IActionResult GetImage(Guid id)
        {          
            var imageFromRepo = _galleryRepository.GetImage(id);

            if (imageFromRepo == null)
            {
                return NotFound();
            }

            var imageToReturn = Mapper.Map<Model.Image>(imageFromRepo);

            return Ok(imageToReturn);
        }

        [HttpPost()]
        [Authorize(Roles = "PayingUser")]
        public IActionResult CreateImage([FromBody] ImageForCreation imageForCreation)
        {
            if (imageForCreation == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                // return 422 - Unprocessable Entity when validation fails
                return new UnprocessableEntityObjectResult(ModelState);
            }

            // Automapper maps only the Title in our configuration
            var imageEntity = Mapper.Map<Entities.Image>(imageForCreation);

            // Create an image from the passed-in bytes (Base64), and 
            // set the filename on the image

            // get this environment's web root path (the path
            // from which static content, like an image, is served)
            var webRootPath = _hostingEnvironment.WebRootPath;

            // create the filename
            string fileName = Guid.NewGuid().ToString() + ".jpg";
            
            // the full file path
            var filePath = Path.Combine($"{webRootPath}/images/{fileName}");

            // write bytes and auto-close stream
            System.IO.File.WriteAllBytes(filePath, imageForCreation.Bytes);

            // fill out the filename
            imageEntity.FileName = fileName;

            // ownerId should be set - can't save image in starter solution, will
            // be fixed during the course
            //imageEntity.OwnerId = ...;

            // set the ownerId on the imageEntity
            var ownerId = User.Claims.FirstOrDefault(c => c.Type == "sub").Value;
            imageEntity.OwnerId = ownerId;

            // add and save.  
            _galleryRepository.AddImage(imageEntity);

            if (!_galleryRepository.Save())
            {
                throw new Exception($"Adding an image failed on save.");
            }

            var imageToReturn = Mapper.Map<Image>(imageEntity);

            return CreatedAtRoute("GetImage",
                new { id = imageToReturn.Id },
                imageToReturn);
        }

        [HttpDelete("{id}")]
        [Authorize("MustOwnImage")]
        public IActionResult DeleteImage(Guid id)
        {
            
            var imageFromRepo = _galleryRepository.GetImage(id);

            if (imageFromRepo == null)
            {
                return NotFound();
            }

            _galleryRepository.DeleteImage(imageFromRepo);

            if (!_galleryRepository.Save())
            {
                throw new Exception($"Deleting image with {id} failed on save.");
            }

            return NoContent();
        }

        [HttpPut("{id}")]
        [Authorize("MustOwnImage")]
        public IActionResult UpdateImage(Guid id, 
            [FromBody] ImageForUpdate imageForUpdate)
        {
           
            if (imageForUpdate == null)
            {
                return BadRequest();
            }
            
            if (!ModelState.IsValid)
            {
                // return 422 - Unprocessable Entity when validation fails
                return new UnprocessableEntityObjectResult(ModelState);
            }

            var imageFromRepo = _galleryRepository.GetImage(id);
            if (imageFromRepo == null)
            {
                return NotFound();
            }

            Mapper.Map(imageForUpdate, imageFromRepo);

            _galleryRepository.UpdateImage(imageFromRepo);

            if (!_galleryRepository.Save())
            {
                throw new Exception($"Updating image with {id} failed on save.");
            }

            return NoContent();
        }

        public async Task WriteOutIdentityInformation()
        {
            // get the saved identity token
            var identityToken = await HttpContext
                .GetTokenAsync(OpenIdConnectParameterNames.IdToken);

            // write it out
            Debug.WriteLine($"Identity token: {identityToken}");

            // write out the user claims
            foreach (var claim in User.Claims)
            {
                Debug.WriteLine($"Claim type: {claim.Type} - Claim value: {claim.Value}");
            }
        }
        [HttpGet("Logout")]
        public async Task Logout()
        {

            // get the metadata
            var discoveryClient = new IdentityModel.Client.DiscoveryClient("https://localhost:44379/");
            var metaDataResponse = await discoveryClient.GetAsync();

            // create a TokenRevocationClient
            var revocationClient = new IdentityModel.Client.TokenRevocationClient(metaDataResponse.RevocationEndpoint, "angular_spa");

            // get the access token to revoke 
            var accessToken = await HttpContext
              .GetTokenAsync(OpenIdConnectParameterNames.AccessToken);

            if (!string.IsNullOrWhiteSpace(accessToken))
            {
                var revokeAccessTokenResponse =
                    await revocationClient.RevokeAccessTokenAsync(accessToken);

                if (revokeAccessTokenResponse.IsError)
                {
                    throw new Exception("Problem encountered while revoking the access token."
                        , revokeAccessTokenResponse.Exception);
                }
            }
            // revoke the refresh token as well
            var refreshToken = await HttpContext
                 .GetTokenAsync(OpenIdConnectParameterNames.RefreshToken);

            if (!string.IsNullOrWhiteSpace(refreshToken))
            {
                var revokeRefreshTokenResponse =
                    await revocationClient.RevokeRefreshTokenAsync(refreshToken);

                if (revokeRefreshTokenResponse.IsError)
                {
                    throw new Exception("Problem encountered while revoking the refresh token."
                        , revokeRefreshTokenResponse.Exception);
                }
            }


            // Clears the  local cookie ("Cookies" must match name from scheme)
            // await HttpContext.SignOutAsync("Cookies");
            //await HttpContext.SignOutAsync("oidc");
        }
        [HttpPost("RevokeAccess")]        
        public async Task<IActionResult> RevokeAccess([FromBody]RevokeAccessViewModel revokeAccessModel)
        {
            string revokeUrl = $"{IDP_BASE_URL}/Account/RevokeUserAccess";
            //using (HttpClient client = new HttpClient())
            System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
            var json = JsonConvert.SerializeObject(revokeAccessModel);
            var stringContent = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

            client.BaseAddress = new Uri(revokeUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            HttpContent contents = new StringContent(json);
            System.Data.DataTable table = null;
            HttpResponseMessage response = await client.PostAsync(revokeUrl, stringContent);
            if (response.IsSuccessStatusCode)
            {
                var data = await response.Content.ReadAsStringAsync();
                table = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Data.DataTable>(data);

            }
            return Ok(table);
        }
    }
}